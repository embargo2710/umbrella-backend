<?php

namespace App\Controller;

use App\Component\UrlShortener;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends Controller
{
    /**
     * @Route("/api/short")
     */
    public function index(Request $request, UrlShortener $urlShortener)
    {
        try {
            $body = json_decode($request->getContent());

            return $this->json([
                'result' => true,
                'short_url' => $urlShortener->short($body->full_url ?? null, $body->short_url ?? null),
            ]);
        } catch (\Throwable $throwable) {
            return $this->json([
                'result' => false,
                'error' => $throwable->getMessage(),
            ]);
        }
    }
}
