<?php

namespace App\Controller;

use App\Component\UrlRedirecter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UrlController
 * @package App\Controller
 */
class UrlController extends Controller
{
    /**
     * @Route("/{url}")
     *
     * @param string $url
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function url(string $url, UrlRedirecter $urlRedirecter)
    {
        try {
            return $urlRedirecter->redirect($url);
        } catch (\Throwable $throwable) {
            throw new NotFoundHttpException($throwable->getMessage());
        }
    }
}
