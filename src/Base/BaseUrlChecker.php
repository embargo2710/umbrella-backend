<?php

namespace App\Base;

/**
 * Class BaseUrlChecker
 * @package App\Base
 */
abstract class BaseUrlChecker
{
    /**
     * @param string $url
     *
     * @return bool
     */
    abstract public function check(string $url): bool;
}
