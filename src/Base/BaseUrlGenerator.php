<?php

namespace App\Base;

/**
 * Class BaseUrlGenerator
 * @package App\Base
 */
abstract class BaseUrlGenerator
{
    /**
     * @return string
     */
    abstract public function generate(): string;
}
