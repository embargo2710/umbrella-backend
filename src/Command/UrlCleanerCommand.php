<?php

namespace App\Command;

use App\Component\UrlCleaner;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UrlCleanerCommand
 * @package App\Command
 */
class UrlCleanerCommand extends Command
{
    /**
     * @var UrlCleaner
     */
    protected $urlCleaner;

    /**
     * UrlCleanerCommand constructor.
     *
     * @param null|string $name
     * @param UrlCleaner $urlCleaner
     */
    public function __construct(?string $name = null, UrlCleaner $urlCleaner)
    {
        $this->urlCleaner = $urlCleaner;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:url-cleaner')
            ->setDescription('Дворник-ссылочник')
            ->setHelp('Эта команда удаляет старые ссылки');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->urlCleaner->clean();
    }
}
