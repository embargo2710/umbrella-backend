<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UrlRepository")
 * @UniqueEntity("short_url")
 */
class Url
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="short_url", type="string", length=13, unique=true)
     * @Assert\Valid()
     */
    private $short_url;

    /**
     * @ORM\Column(type="text")
     */
    private $full_url;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="integer")
     */
    private $uses = 0;

    /**
     * Url constructor.
     */
    public function __construct()
    {
        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUses()
    {
        return $this->uses;
    }

    /**
     * @param mixed $uses
     */
    public function setUses($uses): void
    {
        $this->uses = $uses;
    }

    /**
     * @return string
     */
    public function getShortUrl(): string
    {
        return $this->short_url;
    }

    /**
     * @param string $short_url
     */
    public function setShortUrl($short_url): void
    {
        $this->short_url = $short_url;
    }

    /**
     * @return string
     */
    public function getFullUrl(): string
    {
        return $this->full_url;
    }

    /**
     * @param string $full_url
     */
    public function setFullUrl($full_url): void
    {
        $this->full_url = $full_url;
    }

    public function getId()
    {
        return $this->id;
    }
}
