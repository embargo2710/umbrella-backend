<?php

namespace App\Component;

use App\Entity\Url;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UrlCleaner
 * @package App\Service
 */
class UrlCleaner
{
    /**
     * @var int
     */
    private $period;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UrlCleaner constructor.
     *
     * @param int $period
     */
    public function __construct(int $period = 15, EntityManagerInterface $entityManager)
    {
        $this->period = $period;

        $this->entityManager = $entityManager;
    }

    public function clean(): void
    {
        $repository = $this->entityManager->getRepository(Url::class);

        $repository
            ->createQueryBuilder('q')
            ->delete()
            ->where("q.created_at < DATE_ADD(CURRENT_DATE(), :period, 'day')")
            ->setParameter('period', -$this->period)
            ->getQuery()
            ->execute();
    }
}
