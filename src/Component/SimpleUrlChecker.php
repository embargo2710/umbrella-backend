<?php

namespace App\Component;

use App\Base\BaseUrlChecker;

/**
 * Class SimpleUrlChecker
 * @package App\Component
 */
class SimpleUrlChecker extends BaseUrlChecker
{
    /**
     * @inheritdoc
     */
    public function check(string $url): bool
    {
        $http = curl_init($url);
        curl_setopt($http, CURLOPT_NOBODY, true);
        $result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $result && ($http_status >= 200 && $http_status < 400);
    }
}
