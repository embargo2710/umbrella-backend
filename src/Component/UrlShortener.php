<?php

namespace App\Component;

use App\Entity\Url;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UrlShortener
 * @package App\Component
 */
class UrlShortener
{
    /**
     * @var SimpleUrlChecker
     */
    private $checker;

    /**
     * @var UidUrlGenerator
     */
    private $generator;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UrlShortener constructor.
     *
     * @param SimpleUrlChecker $checker
     * @param UidUrlGenerator $generator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        SimpleUrlChecker $checker,
        UidUrlGenerator $generator,
        EntityManagerInterface $entityManager
    ) {
        $this->checker = $checker;

        $this->generator = $generator;

        $this->entityManager = $entityManager;
    }

    /**
     * @param null|string $full_url
     * @param null|string $short_url
     *
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function short(?string $full_url, ?string $short_url): string
    {
        $url = new Url();

        if (!$full_url) {
            throw new \Exception('Передан пустой url!');
        }

        if (!$this->checker->check($full_url)) {
            throw new \Exception('Передан неверный url!');
        }

        if (!$short_url) {
            $url->setShortUrl($this->generator->generate());
        } else {
            $repository = $this->entityManager->getRepository(Url::class);

            if ($repository->findOneByShortUrl($short_url)) {
                throw new \Exception('Данный короткий урл уже занят!');
            }

            $url->setShortUrl($short_url);
        }

        $url->setFullUrl($full_url);

        $this->entityManager->persist($url);

        $this->entityManager->flush();

        return $url->getShortUrl();
    }
}
