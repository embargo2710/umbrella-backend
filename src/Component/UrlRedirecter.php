<?php

namespace App\Component;

use App\Entity\Url;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UrlRedirecter
 * @package App\Component
 */
class UrlRedirecter
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UrlRedirecter constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $url
     *
     * @return RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function redirect(string $url)
    {
        $repository = $this->entityManager->getRepository(Url::class);

        /**
         * @var Url $url
         */
        $url = $repository->findOneByShortUrl($url);

        if (!$url) {
            throw new NotFoundHttpException('Не найдено!');
        }

        $full_url = $url->getFullUrl();

        if (strpos($full_url, 'http://') === false && strpos($full_url, 'https://') === false) {
            $full_url = 'http://' . $full_url;
        }

        $url->setUses($url->getUses() + 1);

        $this->entityManager->persist($url);

        $this->entityManager->flush();

        return new RedirectResponse($full_url, RedirectResponse::HTTP_MOVED_PERMANENTLY);
    }
}
