<?php

namespace App\Component;

use App\Base\BaseUrlGenerator;

/**
 * Class UidUrlGenerator
 * @package App\Component
 */
class UidUrlGenerator extends BaseUrlGenerator
{
    /**
     * @return string
     */
    public function generate(): string
    {
        return uniqid();
    }
}
